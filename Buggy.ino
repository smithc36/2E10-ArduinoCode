
const int LEYE = 2; // sets name for left eye 
const int REYE = 10; // sets name for right eye 

const int AOne = 5; // sets name for H-bridge input A1 
const int ATwo = 6; // sets name for H-brigde input A2
const int AThree = 12; // sets name for H-bridge input A3 
const int AFour = 9; // Sets name for H-brigde input A4 

const int US_TRIG = 11; // Sets name for Trigger
const int US_ECHO = 3; // Sets name for Echo


void setup() {
  Serial.begin(9600); // Sets serial speed to default of 9600

  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );// Sets eye pins as inputs 
  pinMode( AOne, OUTPUT ); // +Left Motor 
  pinMode( ATwo, OUTPUT ); // -Left Motor 
  pinMode( AThree, OUTPUT ); // -Right motor 
  pinMode( AFour, OUTPUT ); // + Right motor // Sets motorcontol pins to outputs
  pinMode( US_TRIG, OUTPUT );
  pinMode( US_ECHO, INPUT );
  
}


long last_check;
int last_distance; 

void loop() {
  
  long time_since_check = millis();
  int distance;
  long duration;
  
  if ( time_since_check - last_check >= 1000 ) {
    digitalWrite( US_TRIG, LOW );
    delayMicroseconds( 2 );
    digitalWrite( US_TRIG, HIGH );
    delayMicroseconds( 10 );
    digitalWrite( US_TRIG, LOW );

    duration = pulseIn( US_ECHO, HIGH );
    distance = duration / 58;

    if ( distance <= 20 ) {
      analogWrite( AOne, 0 );
      analogWrite( ATwo, 0 );
      analogWrite( AThree, 0 );
      analogWrite( AFour, 0 );
    }
    
    last_check = time_since_check;
    last_distance = distance;

  }


  if ( digitalRead( LEYE ) == LOW && last_distance > 20 ) { // if left eye is LOW send high signal to motor input A1 and LOW to A2
      analogWrite( AOne, 200);
  }
  
  else {
    analogWrite( AOne, 0 );// No power 
  }

  if( digitalRead( REYE ) == LOW && last_distance > 20 ) { // if left eye is LOW send high signal to motor input A4 and LOW to A3
    analogWrite( AFour, 200);
  }
  
  else{
    analogWrite( AFour, 0 ); // no power 
  }


  delay(25); // set delay to 25ms 

}
